package com.rostyslavprotsiv;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.model.entity.Bot;
import com.rostyslavprotsiv.model.entity.Playground;
import com.rostyslavprotsiv.model.exception.PlaygroundLogicalException;
import com.rostyslavprotsiv.model.exception.PlaygroundTechnicalException;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
//        Controller controller = new Controller();
//        try {
//            controller.play();
//        } catch (PlaygroundLogicalException | PlaygroundTechnicalException e) {
//            System.err.println(e.getMessage() + " cause: " + e.getCause());
//        }
        try(Bot bot = new Bot(new Playground(), "aaa");
            Bot bot1 = new Bot(new Playground(), "bbb");
            Bot bot2 = new Bot(new Playground(), "ccc");
            Bot bot3 = new Bot(new Playground(), "ddd")) {
            System.out.println(bot.getName() + " second botname: " + bot1.getName());
            throw new NullPointerException();
        } catch(NullPointerException | CloneNotSupportedException e) {
            System.out.println(Arrays.toString(e.getSuppressed()));
        }
    }
}

package com.rostyslavprotsiv.view;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
    private static final Scanner SCANNER = new Scanner(System.in);

    public void welcome(String opponentName, String playerName) {
        System.out.println("Hi " + playerName + ", welcome to our game!");
        System.out.println("You will play with " + opponentName);
        System.out.println("Let's roll!");
        System.out.println("Press something to continue...");
        SCANNER.nextLine();
    }

    public void outGame(String game) {
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println(game);
    }

    public int inputRow() throws InputMismatchException {
        System.out.println("Please, input a row number: ");
        return SCANNER.nextInt();
    }

    public int inputColumn() throws InputMismatchException {
        System.out.println("Please, input a column number: ");
        return SCANNER.nextInt();
    }

    public void informAboutTurn(String name) {
        System.out.println(name + ", it is your turn:");
    }

    public void congratulateAboutFirstTurn(String name) {
        System.out.println("Congratulation " + name + ", your move is first!!");
    }

    public void congratulateWinner(String name) {
        System.err.println("Congratulations!! " + name + " WON!!!");
    }

    public void informAboutDraw() {
        System.err.println("Unfortunately, there is no winner in this game");
    }
}

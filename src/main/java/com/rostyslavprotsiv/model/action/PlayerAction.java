package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Player;
import com.rostyslavprotsiv.model.exception.PlaygroundLogicalException;

public class PlayerAction {
    public void makeMove(Player player, int row,
                            int column) throws PlaygroundLogicalException {
        player.getPlayground().setCell(true, row, column);
    }
}

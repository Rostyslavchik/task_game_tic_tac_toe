package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.Bot;
import com.rostyslavprotsiv.model.exception.PlaygroundLogicalException;

import static com.rostyslavprotsiv.model.entity.Playground.*;

public class BotAction {
    public static final int CENTRAL_ROW = 1;
    public static final int CENTRAL_COLUMN = 1;
    public static final int LEFT_TOP_CORNER_ROW = 0;
    public static final int LEFT_TOP_CORNER_COLUMN = 0;
    public static final int RIGHT_TOP_CORNER_ROW = 0;
    public static final int RIGHT_TOP_CORNER_COLUMN = 2;
    public static final int LEFT_BOTTOM_CORNER_ROW = 2;
    public static final int LEFT_BOTTOM_CORNER_COLUMN = 0;
    public static final int RIGHT_BOTTOM_CORNER_ROW = 2;
    public static final int RIGHT_BOTTOM_CORNER_COLUMN = 2;
    public static final int MIDDLE_TOP_ROW = 0;
    public static final int MIDDLE_TOP_COLUMN = 1;
    private static boolean iKnowThirdTurn = false;

    public void makeMove(Bot bot) throws PlaygroundLogicalException {
        int[] coordinates = getWinningPosition();
        if (coordinates != null) {
            bot.getPlayground().setCell(false,
                                coordinates[0], coordinates[1]);
            return;
        }
        coordinates = getLosingPosition();
        if (coordinates != null) {
            bot.getPlayground().setCell(false,
                                coordinates[0], coordinates[1]);
            return;
        }
        coordinates = chooseMoveCoordinates();
        if (coordinates != null) {
            bot.getPlayground().setCell(false,
                                coordinates[0], coordinates[1]);
            return;
        }
    }

    private int[] getWinningPosition() {
        int[] coordinates;
        if ((coordinates = getForHorizontalCase()) != null) {
            return coordinates;
        } else if ((coordinates = getForVerticalCase()) != null) {
            return coordinates;
        } else if ((coordinates = getForDiagonalCase()) != null) {
            return coordinates;
        }
        return null;
    }

    private int[] getForHorizontalCase() {
        int[] winningCoordinates = new int[2];
        int previousRow = -1;
        int thirdCellRow;
        for (int j = 0; j < MAX_COLUMNS; j++) {
            for (int i = 0; i < MAX_ROWS; i++) {
                if (Boolean.FALSE.equals(AREA[i][j])) {
                    if (previousRow != -1) {
                        thirdCellRow = MAX_ROWS - (previousRow + i);
                        if (AREA[thirdCellRow][j] == null) {
                            winningCoordinates[0] = thirdCellRow;
                            winningCoordinates[1] = j;
                            return winningCoordinates;
                        }
                    }
                    previousRow = i;
                }
            }
            previousRow = -1;
        }
        return null;
    }

    private int[] getForVerticalCase() {
        int[] winningCoordinates = new int[2];
        int previousColumn = -1;
        int thirdCellColumn;
        for (int i = 0; i < MAX_ROWS; i++) {
            for (int j = 0; j < MAX_COLUMNS; j++) {
                if (Boolean.FALSE.equals(AREA[i][j])) {
                    if (previousColumn != -1) {
                        thirdCellColumn = MAX_COLUMNS - (previousColumn + j);
                        if (AREA[i][thirdCellColumn] == null) {
                            winningCoordinates[0] = i;
                            winningCoordinates[1] = thirdCellColumn;
                            return winningCoordinates;
                        }
                    }
                    previousColumn = j;
                }
            }
            previousColumn = -1;
        }
        return null;
    }

    private int[] getForDiagonalCase() {
        int[] winningCoordinates = new int[2];
        int previous = -1;
        int thirdCell;
        //first case
        for (int i = 0; i < MAX_ROWS; i ++) {
            if (Boolean.FALSE.equals(AREA[i][i])) {
                if (previous != -1) {
                    thirdCell = MAX_ROWS - (previous + i);
                    if (AREA[thirdCell][thirdCell] == null) {
                        winningCoordinates[0] = thirdCell;
                        winningCoordinates[1] = thirdCell;
                        return winningCoordinates;
                    }
                }
                previous = i;
            }
        }
        previous = -1;
        //second case
        for (int i = 0; i < MAX_ROWS; i ++) {
            if (Boolean.FALSE.equals(AREA[i][MAX_ROWS - i - 1])) {
                if (previous != -1) {
                    thirdCell = MAX_ROWS - (previous + i);
                    if (AREA[thirdCell][MAX_ROWS - thirdCell - 1] == null) {
                        winningCoordinates[0] = thirdCell;
                        winningCoordinates[1] = MAX_ROWS - thirdCell - 1;
                        return winningCoordinates;
                    }
                }
                previous = i;
            }
        }
        return null;
    }

    //------------------------------------------------------------------------
    //Opposite methods
    private int[] getLosingPosition() {
        int[] coordinates;
        if ((coordinates = getLosingHorizontalCase()) != null) {
            return coordinates;
        } else if ((coordinates = getLosingVerticalCase()) != null) {
            return coordinates;
        } else if ((coordinates = getLosingDiagonalCase()) != null) {
            return coordinates;
        }
        return null;
    }

    private int[] getLosingHorizontalCase() {
        int[] losingCoordinates = new int[2];
        int previousRow = -1;
        int thirdCellRow;
        for (int j = 0; j < MAX_COLUMNS; j++) {
            for (int i = 0; i < MAX_ROWS; i++) {
                if (Boolean.TRUE.equals(AREA[i][j])) {
                    if (previousRow != -1) {
                        thirdCellRow = MAX_ROWS - (previousRow + i);
                        if (AREA[thirdCellRow][j] == null) {
                            losingCoordinates[0] = thirdCellRow;
                            losingCoordinates[1] = j;
                            return losingCoordinates;
                        }
                    }
                    previousRow = i;
                }
            }
            previousRow = -1;
        }
        return null;
    }

    private int[] getLosingVerticalCase() {
        int[] losingCoordinates = new int[2];
        int previousColumn = -1;
        int thirdCellColumn;
        for (int i = 0; i < MAX_ROWS; i++) {
            for (int j = 0; j < MAX_COLUMNS; j++) {
                if (Boolean.TRUE.equals(AREA[i][j])) {
                    if (previousColumn != -1) {
                        thirdCellColumn = MAX_COLUMNS - (previousColumn + j);
                        if (AREA[i][thirdCellColumn] == null) {
                            losingCoordinates[0] = i;
                            losingCoordinates[1] = thirdCellColumn;
                            return losingCoordinates;
                        }
                    }
                    previousColumn = j;
                }
            }
            previousColumn = -1;
        }
        return null;
    }

    private int[] getLosingDiagonalCase() {
        int[] losingCoordinates = new int[2];
        int previous = -1;
        int thirdCell;
        //first case
        for (int i = 0; i < MAX_ROWS; i ++) {
            if (Boolean.TRUE.equals(AREA[i][i])) {
                if (previous != -1) {
                    thirdCell = MAX_ROWS - (previous + i);
                    if (AREA[thirdCell][thirdCell] == null) {
                        losingCoordinates[0] = thirdCell;
                        losingCoordinates[1] = thirdCell;
                        return losingCoordinates;
                    }
                }
                previous = i;
            }
        }
        previous = -1;
        //second case
        for (int i = 0; i < MAX_ROWS; i ++) {
            if (Boolean.TRUE.equals(AREA[i][MAX_ROWS - i - 1])) {
                if (previous != -1) {
                    thirdCell = MAX_ROWS - (previous + i);
                    if (AREA[thirdCell][MAX_ROWS - thirdCell - 1] == null) {
                        losingCoordinates[0] = thirdCell;
                        losingCoordinates[1] = MAX_ROWS - thirdCell - 1;
                        return losingCoordinates;
                    }
                }
                previous = i;
            }
        }
        return null;
    }
    //------------------------------------------------------------------------

    private int[] chooseMoveCoordinates() {
        int[] coordinates;
        if ((coordinates = chooseNotLoosingCoordinates()) != null) {
            System.out.println("chooseNotLoosingCoordinate");
            return coordinates;
        }
        if ((coordinates = getHorizontalCoordinates()) != null) {
            System.out.println("getHorizontalCoordinates");
            return coordinates;
        }
        if ((coordinates = getVerticalCoordinates()) != null) {
            System.out.println("getVerticalCoordinates()");
            return coordinates;
        }
        if ((coordinates = getDiagonalCoordinates()) != null) {
            System.out.println("getDiagonalCoordinates");
            return coordinates;
        }
        if ((coordinates = chooseRandomCoordinates()) != null) {
            System.out.println("chooseRandomCoordinates");
            return coordinates;
        }
        return null;
    }

   //-------------------------------------------------------------------------
    private int[] getHorizontalCoordinates() {
        int[] nextMoveCoordinates = new int[2];
        for (int j = 0; j < MAX_COLUMNS; j++) {
            for (int i = 0; i < MAX_ROWS; i++) {
                if (Boolean.FALSE.equals(AREA[i][j])) {
                    if (i == 0) {
                        if (AREA[i + 1][j] == null && AREA[i + 2][j] == null) {
                            nextMoveCoordinates[0] = i + 1;
                            nextMoveCoordinates[1] = j;
                            return nextMoveCoordinates;
                        }
                    }
                    if (i == 1) {
                        if (AREA[i - 1][j] == null && AREA[i + 1][j] == null) {
                            nextMoveCoordinates[0] = i - 1;
                            nextMoveCoordinates[1] = j;
                            return nextMoveCoordinates;
                        }
                    }
                    if (i == 2) {
                        if (AREA[i - 1][j] == null && AREA[i - 2][j] == null) {
                            nextMoveCoordinates[0] = i - 1;
                            nextMoveCoordinates[1] = j;
                            return nextMoveCoordinates;
                        }
                    }
                }
            }
        }
        return null;
    }

    private int[] getVerticalCoordinates() {
        int[] nextMoveCoordinates = new int[2];
        for (int i = 0; i < MAX_ROWS; i++) {
            for (int j = 0; j < MAX_COLUMNS; j++) {
                if (Boolean.FALSE.equals(AREA[i][j])) {
                    if (j == 0) {
                        if (AREA[i][j + 1] == null && AREA[i][j + 2] == null) {
                            nextMoveCoordinates[0] = i;
                            nextMoveCoordinates[1] = j + 1;
                            return nextMoveCoordinates;
                        }
                    }
                    if (j == 1) {
                        if (AREA[i][j - 1] == null && AREA[i][j + 1] == null) {
                            nextMoveCoordinates[0] = i;
                            nextMoveCoordinates[1] = j - 1;
                            return nextMoveCoordinates;
                        }
                    }
                    if (j == 2) {
                        if (AREA[i][j - 1] == null && AREA[i][j - 2] == null) {
                            nextMoveCoordinates[0] = i;
                            nextMoveCoordinates[1] = j - 1;
                            return nextMoveCoordinates;
                        }
                    }
                }
            }
        }
        return null;
    }

    private int[] getDiagonalCoordinates() {
        int coordinates[];
        if ((coordinates = getDescendingDiagonalCoordinates()) != null) {
            return coordinates;
        }
        if ((coordinates = getAscendingDiagonalCoordinates()) != null) {
            return coordinates;
        }
        return null;
    }

    private int[] getDescendingDiagonalCoordinates() {
        int[] nextMoveCoordinates = new int[2];
        for (int i = 0; i < MAX_ROWS; i++) {
            if (Boolean.FALSE.equals(AREA[i][i])) {
                if (i == 0) {
                    if (AREA[i + 1][i + 1] == null
                            && AREA[i + 2][i + 2] == null) {
                        nextMoveCoordinates[0] = i + 1;
                        nextMoveCoordinates[1] = i + 1;
                        return nextMoveCoordinates;
                    }
                }
                if (i == 1) {
                    if (AREA[i - 1][i - 1] == null
                            && AREA[i + 1][i + 1] == null) {
                        nextMoveCoordinates[0] = i - 1;
                        nextMoveCoordinates[1] = i - 1;
                        return nextMoveCoordinates;
                    }
                }
                if (i == 2) {
                    if (AREA[i - 1][i - 1] == null
                            && AREA[i - 2][i - 2] == null) {
                        nextMoveCoordinates[0] = i - 1;
                        nextMoveCoordinates[1] = i - 1;
                        return nextMoveCoordinates;
                    }
                }
            }
        }
        return null;
    }

    private int[] getAscendingDiagonalCoordinates() {
        int[] nextMoveCoordinates = new int[2];
        for (int i = 0; i < MAX_ROWS; i++) {
            if (Boolean.FALSE.equals(AREA[i][MAX_ROWS - i - 1])) {
                if (i == 0) {
                    if (AREA[i + 1][MAX_ROWS - i - 2] == null
                            && AREA[i + 2][MAX_ROWS - i - 3] == null) {
                        nextMoveCoordinates[0] = i + 1;
                        nextMoveCoordinates[1] = MAX_ROWS - i - 2;
                        return nextMoveCoordinates;
                    }
                }
                if (i == 1) {
                    if (AREA[i - 1][MAX_ROWS - i] == null
                            && AREA[i + 1][MAX_ROWS - i - 2] == null) {
                        nextMoveCoordinates[0] = i - 1;
                        nextMoveCoordinates[1] = MAX_ROWS - i;
                        return nextMoveCoordinates;
                    }
                }
                if (i == 2) {
                    if (AREA[i - 1][MAX_ROWS - i] == null
                            && AREA[i - 2][MAX_ROWS - i + 1] == null) {
                        nextMoveCoordinates[0] = i - 1;
                        nextMoveCoordinates[1] = i - 1;
                        return nextMoveCoordinates;
                    }
                }
            }
        }
        return null;
    }

    private int[] chooseRandomCoordinates() {
        int coordinates[] = new int[2];
        for (int i = 0; i < MAX_ROWS; i ++) {
            for (int j = 0; j < MAX_COLUMNS; j ++) {
                if (AREA[i][j] == null) {
                    coordinates[0] = i;
                    coordinates[1] = j;
                    return coordinates;
                }
            }
        }
        return null;
    }

    private int[] chooseNotLoosingCoordinates() {
        int[] coordinates;
        if ((coordinates = getFirstTurnCoordinates()) != null) {
            System.out.println("getFirstTurnCoordinates");
            return coordinates;
        }
        if ((coordinates = getSecondTurnCoordinates()) != null) {
            System.out.println("getSecondTurnCoordinates");
            return coordinates;
        }
        if ((coordinates = getThirdCornerCoordinates()) != null) {
            System.out.println("getThirdCornerCoordinates");
            return coordinates;
        }
        if ((coordinates = getSecondCornerCoordinates()) != null) {
            System.out.println("getSecondCornerCoordinates");
            return coordinates;
        }
        return null;
    }

    private int[] getFirstTurnCoordinates() {
        int coordinates[] = new int[2];
        boolean firstMoveIsMade = false;
        for (int i = 0; i < MAX_ROWS; i ++) {
            for (int j = 0; j < MAX_COLUMNS; j ++) {
                if (AREA[i][j] != null) {
                    firstMoveIsMade = true;
                }
            }
        }
        if (!firstMoveIsMade) {
            coordinates[0] = CENTRAL_ROW;
            coordinates[1] = CENTRAL_COLUMN;
            return coordinates;
        }
        return null;
    }

    private int[] getSecondTurnCoordinates() {
        int coordinates[] = new int[2];
        boolean firstMoveIsMade = false;
        boolean movedToCenter = false;
        for (int i = 0; i < MAX_ROWS; i ++) {
            for (int j = 0; j < MAX_COLUMNS; j ++) {
                if (AREA[i][j] != null
                        && (i != CENTRAL_ROW) && (j != CENTRAL_COLUMN)) {
                    firstMoveIsMade = true;
                }
                if (Boolean.TRUE.equals(AREA[i][j])
                        && (i == CENTRAL_ROW) && (j == CENTRAL_COLUMN)) {
                    movedToCenter = true;
                }
            }
        }
        if (!firstMoveIsMade && movedToCenter) {
            coordinates[0] = LEFT_TOP_CORNER_ROW;
            coordinates[1] = LEFT_TOP_CORNER_COLUMN;
            return coordinates;
        }
        return null;
    }

    private int[] getSecondCornerCoordinates() {
        int coordinates[] = new int[2];
        boolean firstMoveIsMade = false;
        boolean movedToCorner = false;
        for (int i = 0; i < MAX_ROWS; i ++) {
            for (int j = 0; j < MAX_COLUMNS; j ++) {
                if (wasFirstMoveMadeNotToCorner(i, j)) {
                    firstMoveIsMade = true;
                }
                if (wasFirstMoveMadeToCorner(i, j)) {
                    movedToCorner = true;
                }
            }
        }
        if (!firstMoveIsMade && movedToCorner) {
            coordinates[0] = CENTRAL_ROW;
            coordinates[1] = CENTRAL_COLUMN;
            iKnowThirdTurn = true;
            return coordinates;
        }
        return null;
    }

    private int[] getThirdCornerCoordinates() {
        int coordinates[] = new int[2];
        int totalMoves = 0;
        if (iKnowThirdTurn) {
            for (int i = 0; i < MAX_ROWS; i++) {
                for (int j = 0; j < MAX_COLUMNS; j++) {
                    if (AREA[i][j] != null) {
                        totalMoves++;
                    }
                }
            }
            if (totalMoves == 3) {
                coordinates[0] = MIDDLE_TOP_ROW;
                coordinates[1] = MIDDLE_TOP_COLUMN;
                return coordinates;
            }
        }
        iKnowThirdTurn = false;
        return null;
    }

    private boolean wasFirstMoveMadeNotToCorner(int i, int j) {
        return (AREA[i][j] != null
                && (i != LEFT_TOP_CORNER_ROW)
                && (j != LEFT_TOP_CORNER_COLUMN)
                && (i != RIGHT_TOP_CORNER_ROW)
                && (j != RIGHT_TOP_CORNER_COLUMN)
                && (i != LEFT_BOTTOM_CORNER_ROW)
                && (j != LEFT_BOTTOM_CORNER_COLUMN)
                && (i != RIGHT_BOTTOM_CORNER_ROW)
                && (j != RIGHT_BOTTOM_CORNER_COLUMN));
    }

    private boolean wasFirstMoveMadeToCorner(int i, int j) {
        return (Boolean.TRUE.equals(AREA[i][j])
                && (((i == LEFT_TOP_CORNER_ROW)
                && (j == LEFT_TOP_CORNER_COLUMN))
                || ((i == RIGHT_TOP_CORNER_ROW)
                && (j == RIGHT_TOP_CORNER_COLUMN))
                || ((i == LEFT_BOTTOM_CORNER_ROW)
                && (j == LEFT_BOTTOM_CORNER_COLUMN))
                || ((i == RIGHT_BOTTOM_CORNER_ROW)
                && (j == RIGHT_BOTTOM_CORNER_COLUMN))));
    }
}

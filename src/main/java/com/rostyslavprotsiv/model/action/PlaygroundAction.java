package com.rostyslavprotsiv.model.action;

import java.util.Random;

import static com.rostyslavprotsiv.model.entity.Playground.*;

public class PlaygroundAction {
    private static final Random RANDOM = new Random();

    public Boolean whoIsWinner() {
        for (int i = 0; i < MAX_COLUMNS; i ++) {
            if (isThreeInAColumnTrue(i)) {
                return true;
            }
            if (isThreeInAColumnFalse(i)) {
                return false;
            }
            if (isThreeInARowTrue(i)) {
                return true;
            }
            if (isThreeInARowFalse(i)) {
                return false;
            }
        }
        if (isThreeInDescendingDiagonalTrue()) {
            return true;
        }
        if (isThreeInDescendingDiagonalFalse()) {
            return false;
        }
        if (isThreeInAscendingDiagonalTrue()) {
            return true;
        }
        if (isThreeInAscendingDiagonalFalse()) {
            return false;
        }
        return null;
    }

    public boolean whoIsFirst() {
        return RANDOM.nextBoolean();
    }

    private boolean isThreeInAColumnTrue(int column) {
        return (Boolean.TRUE.equals(AREA[0][column])
                && Boolean.TRUE.equals(AREA[1][column])
                && Boolean.TRUE.equals(AREA[2][column]));
    }

    private boolean isThreeInAColumnFalse(int column) {
        return (Boolean.FALSE.equals(AREA[0][column])
                && Boolean.FALSE.equals(AREA[1][column])
                && Boolean.FALSE.equals(AREA[2][column]));
    }

    private boolean isThreeInARowTrue(int row) {
        return (Boolean.TRUE.equals(AREA[row][0])
                && Boolean.TRUE.equals(AREA[row][1])
                && Boolean.TRUE.equals(AREA[row][2]));
    }

    private boolean isThreeInARowFalse(int row) {
        return (Boolean.FALSE.equals(AREA[row][0])
                && Boolean.FALSE.equals(AREA[row][1])
                && Boolean.FALSE.equals(AREA[row][2]));
    }

    private boolean isThreeInDescendingDiagonalTrue() {
        return (Boolean.TRUE.equals(AREA[0][0])
                && Boolean.TRUE.equals(AREA[1][1])
                && Boolean.TRUE.equals(AREA[2][2]));
    }

    private boolean isThreeInDescendingDiagonalFalse() {
        return (Boolean.FALSE.equals(AREA[0][0])
                && Boolean.FALSE.equals(AREA[1][1])
                && Boolean.FALSE.equals(AREA[2][2]));
    }

    private boolean isThreeInAscendingDiagonalTrue() {
        return (Boolean.TRUE.equals(AREA[0][2])
                && Boolean.TRUE.equals(AREA[1][1])
                && Boolean.TRUE.equals(AREA[2][0]));
    }

    private boolean isThreeInAscendingDiagonalFalse() {
        return (Boolean.FALSE.equals(AREA[0][2])
                && Boolean.FALSE.equals(AREA[1][1])
                && Boolean.FALSE.equals(AREA[2][0]));
    }
}

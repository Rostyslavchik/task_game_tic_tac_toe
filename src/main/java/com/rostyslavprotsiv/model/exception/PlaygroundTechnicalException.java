package com.rostyslavprotsiv.model.exception;

public class PlaygroundTechnicalException extends PlaygroundException {
    public PlaygroundTechnicalException() {}

    public PlaygroundTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlaygroundTechnicalException(String message) {
        super(message);
    }

    public PlaygroundTechnicalException(Throwable cause) {
        super(cause);
    }
}

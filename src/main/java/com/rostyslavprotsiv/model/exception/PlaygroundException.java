package com.rostyslavprotsiv.model.exception;

public class PlaygroundException extends Exception {
    public PlaygroundException() {}

    public PlaygroundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlaygroundException(String message) {
        super(message);
    }

    public PlaygroundException(Throwable cause) {
        super(cause);
    }
}

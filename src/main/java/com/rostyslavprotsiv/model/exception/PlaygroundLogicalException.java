package com.rostyslavprotsiv.model.exception;

public class PlaygroundLogicalException extends PlaygroundException {
    public PlaygroundLogicalException() {}

    public PlaygroundLogicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlaygroundLogicalException(String message) {
        super(message);
    }

    public PlaygroundLogicalException(Throwable cause) {
        super(cause);
    }
}

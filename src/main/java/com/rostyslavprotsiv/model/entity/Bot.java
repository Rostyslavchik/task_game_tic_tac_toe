package com.rostyslavprotsiv.model.entity;

public class Bot extends Entity implements AutoCloseable {
    private static final double HASHCODE_INDEX = 34.21;

    public Bot() {}

    public Bot(Playground playground, String name) {
        super(playground, name);
    }

    @Override
    public void close() throws CloneNotSupportedException {
        System.out.println("The bot " + super.getName() + " is closed");
        throw new CloneNotSupportedException();
    }

    @Override
    public int hashCode() {
        return (int) (super.hashCode() * HASHCODE_INDEX);
    }
}

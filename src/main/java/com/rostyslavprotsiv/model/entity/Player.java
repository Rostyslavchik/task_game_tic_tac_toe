package com.rostyslavprotsiv.model.entity;

public class Player extends Entity {
    private static final double HASHCODE_INDEX = 13.77;

    public Player() {}

    public Player(Playground playground, String name) {
        super(playground, name);
    }

    @Override
    public int hashCode() {
        return (int) (super.hashCode() * HASHCODE_INDEX);
    }
}

package com.rostyslavprotsiv.model.entity;

import com.rostyslavprotsiv.model.exception.PlaygroundLogicalException;

public class Playground {
    public static final int MAX_ROWS = 3;
    public static final int MAX_COLUMNS = 3;
    public static final Boolean[][] AREA = new Boolean[MAX_ROWS][MAX_COLUMNS];

    public Playground() {}

    public void setCell(boolean isX, int row, int column)
                            throws PlaygroundLogicalException {
        if (checkCoordinates(row, column) && checkIfFilled(row, column)) {
            AREA[row][column] = isX;
        } else {
            throw new PlaygroundLogicalException("Coordinates for"
                    + " cell are wrong");
        }
    }

    private boolean checkCoordinates(int row, int column) {
        return ((row >= 0 && row < MAX_ROWS)
                    && (column >= 0 && column < MAX_COLUMNS));
    }

    private boolean checkIfFilled(int row, int column) {
        return (AREA[row][column] == null);
    }

    @Override
    public String toString() {
        StringBuilder areaRepresentation = new StringBuilder("_________\n");
        areaRepresentation.append("| |1|2|3|\n");
        for (int i = 0; i < MAX_ROWS; i ++) {
            for (int j = 0; j < MAX_COLUMNS; j ++) {
                if (j == 0) {
                    areaRepresentation.append("|").append(i + 1);
                }
                if (AREA[i][j] == null) {
                    areaRepresentation.append("|").append(" ");
                } else if (AREA[i][j]) {
                    areaRepresentation.append("|").append("x");
                } else {
                    areaRepresentation.append("|").append("o");
                }
            }
            areaRepresentation.append("|").append("\n");
        }
        areaRepresentation.append("| |1|2|3|\n");
        areaRepresentation.append("________|\n");
        return areaRepresentation.toString();
    }
}

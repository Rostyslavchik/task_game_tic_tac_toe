package com.rostyslavprotsiv.model.entity;

public abstract class Entity {
    private Playground playground;
    private String name;
    private boolean lastMoved;

    public Entity() {}

    public Entity(Playground playground, String name) {
        this.playground = playground;
        this.name = name;
    }

    public Playground getPlayground() {
        return playground;
    }

    public void setPlayground(Playground playground) {
        this.playground = playground;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLastMoved() {
        return lastMoved;
    }

    public void setLastMoved(boolean lastMoved) {
        this.lastMoved = lastMoved;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Entity other = (Entity) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int) ((name == null) ? 0 : name.hashCode());
    }

    @Override
    public String toString() {
        return getClass().getName() + "@" + "name = " + name;
    }
}

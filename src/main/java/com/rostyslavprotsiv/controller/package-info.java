/**
 * This module is representing a Controller abstraction in MVC.
 *
 * @author Rostyslav Protsiv
 * @version 1.0.0
 * @since 11.08.2021
 */
package com.rostyslavprotsiv.controller;

package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.BotAction;
import com.rostyslavprotsiv.model.action.PlayerAction;
import com.rostyslavprotsiv.model.action.PlaygroundAction;
import com.rostyslavprotsiv.model.entity.Bot;
import com.rostyslavprotsiv.model.entity.Player;
import com.rostyslavprotsiv.model.entity.Playground;
import com.rostyslavprotsiv.model.exception.PlaygroundLogicalException;
import com.rostyslavprotsiv.model.exception.PlaygroundTechnicalException;
import com.rostyslavprotsiv.view.Menu;
import java.util.InputMismatchException;

import static com.rostyslavprotsiv.model.entity.Playground.MAX_COLUMNS;
import static com.rostyslavprotsiv.model.entity.Playground.MAX_ROWS;

public class Controller {
    private static final Menu MENU = new Menu();
    private static final BotAction BOT_ACTION = new BotAction();
    private static final PlayerAction PLAYER_ACTION = new PlayerAction();
    private static final Playground PLAYGROUND = new Playground();
    private static final Bot BOT = new Bot(PLAYGROUND, "Kysychok");
    private static final Player PLAYER = new Player(PLAYGROUND, "Ross");
    private static final PlaygroundAction PLAYGROUND_ACTION
            = new PlaygroundAction();

    public void play() throws PlaygroundLogicalException,
                            PlaygroundTechnicalException {
        int counter = 1;
        MENU.welcome(BOT.getName(), PLAYER.getName());
        MENU.outGame(PLAYGROUND.toString());
        if (PLAYGROUND_ACTION.whoIsFirst()) {
            MENU.congratulateAboutFirstTurn(PLAYER.getName());
            playersTurn();
        } else {
            MENU.congratulateAboutFirstTurn(BOT.getName());
            botsTurn();
        }
        MENU.outGame(PLAYGROUND.toString());
        while (counter < (MAX_ROWS * MAX_COLUMNS)) {
            if (BOT.isLastMoved()) {
                playersTurn();
                MENU.outGame(PLAYGROUND.toString());
                if (Boolean.TRUE.equals(PLAYGROUND_ACTION.whoIsWinner())) {
                    MENU.congratulateWinner(PLAYER.getName());
                    return;
                }
            } else if (PLAYER.isLastMoved()) {
                botsTurn();
                MENU.outGame(PLAYGROUND.toString());
                if (Boolean.FALSE.equals(PLAYGROUND_ACTION.whoIsWinner())) {
                    MENU.congratulateWinner(BOT.getName());
                    return;
                }
            }
            counter++;
        }
        MENU.informAboutDraw();
    }

    public void playersTurn() throws PlaygroundLogicalException,
                                    PlaygroundTechnicalException {
        int row, column;
        MENU.informAboutTurn(PLAYER.getName());
        try {
            row = MENU.inputRow() - 1;
            column = MENU.inputColumn() - 1;
        } catch (InputMismatchException e) {
            throw new PlaygroundTechnicalException(
                    "The input should be a sufficient number", e);
        }
        PLAYER_ACTION.makeMove(PLAYER, row, column);
        PLAYER.setLastMoved(true);
        BOT.setLastMoved(false);
    }

    public void botsTurn() throws PlaygroundLogicalException {
        MENU.informAboutTurn(BOT.getName());
        BOT_ACTION.makeMove(BOT);
        BOT.setLastMoved(true);
        PLAYER.setLastMoved(false);
    }
}
